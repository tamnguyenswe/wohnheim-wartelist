Too tired of checking the dorm-application waitlist everyday, I wrote myself a small script to fetch my waitlist positions and push the data to thingspeak to visualize 

### Requirement:

- **Python 3.6 or above** 
- **Beautiful Soup 4**:
	```bash
	pip install bs4	
	```
- **Requests**
	```bash
	pip install requests
	```
* use *pip3* instead of *pip* if default python version < 3.6

### The results will look something like this:
> Turmstraße 1 (Hilton) (Wohngemeinschaft) : 542  
Turmstraße 1 (Hilton) (Einzelapartment) : 741  
Bärenstraße 5 (Wohngemeinschaft) : 405  
Bärenstraße 5 (Einzelapartment) : 787  
Bärenstraße 19-21 (Wohngemeinschaft) : 406  
Seilgraben 34-36 (Einzelzimmer) : 310  
Bayernallee 7 (Einzelzimmer) : 207  
Schillerstraße 86-88 (Einzelzimmer) : 218  

#### After visualizing with thingspeak:
[https://thingspeak.com/channels/755154](https://thingspeak.com/channels/755154)

![enter image description here](https://i.imgur.com/dOvKvSm.png)